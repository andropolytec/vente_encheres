package com.example.ramzi.myapplication.Package_Classe;

import java.util.ArrayList;

/**
 * Created by ramzi on 31/03/16.
 */
public class Categorie
{
    public static ArrayList<Categorie> _List_CATEGORIE = new ArrayList<Categorie>();

    private String _id_categorie;
    private String _nom;

    public Categorie(String _id_categorie, String _nom)
    {
        this._id_categorie = _id_categorie;
        this._nom = _nom;
    }

    public String get_id_categorie() {
        return _id_categorie;
    }

    public void set_id_categorie(String _id_categorie) {
        this._id_categorie = _id_categorie;
    }

    public String get_nom() {
        return _nom;
    }

    public void set_nom(String _nom) {
        this._nom = _nom;
    }
}
