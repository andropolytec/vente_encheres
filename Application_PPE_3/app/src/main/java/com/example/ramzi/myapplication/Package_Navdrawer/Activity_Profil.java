package com.example.ramzi.myapplication.Package_Navdrawer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ramzi.myapplication.Package_Classe.Personne;
import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.Package_TabsConnexionInscription.Activity_Tabs;
import com.example.ramzi.myapplication.Package_utile.Utiles;
import com.example.ramzi.myapplication.R;

import java.io.IOException;
import java.util.HashMap;

public class Activity_Profil extends AppCompatActivity
{
    private Toolbar toolbar;
    private  String _imageProfilString = "";

    private ImageView imageView_imageProfil;
    private EditText editText_nom;
    private EditText editText_prenom;
    private EditText editText_password;
    private EditText editText_repetezpassword ;
    private EditText editText_email ;
    private EditText editText_numerocarteidentite ;
    private EditText editText_numerotelphone ;
    private EditText editText_adresse ;
    private Button button_updateInformation;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        toolbar = (Toolbar) findViewById(R.id.navdrawer_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageView_imageProfil = (ImageView) findViewById(R.id.activityprofil_imageview_image);
        editText_nom = (EditText) findViewById(R.id.activityprofil_edittext_name);
        editText_prenom = (EditText) findViewById(R.id.activityprofil_edittext_prenom);
        editText_password = (EditText) findViewById(R.id.activityprofil_edittext_password);
        editText_repetezpassword = (EditText) findViewById(R.id.activityprofil_edittext_repetezpassword);
        editText_email = (EditText) findViewById(R.id.activityprofil_edittext_email);
        editText_numerocarteidentite = (EditText) findViewById(R.id.activityprofil_edittext_numcarteidentite);
        editText_numerotelphone = (EditText) findViewById(R.id.activityprofil_edittext_numerotelephoen);
        editText_adresse = (EditText) findViewById(R.id.activityprofil_edittext_adresse);
        button_updateInformation= (Button) findViewById(R.id.activityprofil_button_updateinformation);
        imageView_imageProfil.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                choisirImage();
            }
        });
        button_updateInformation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Utiles utiles = new Utiles();
                String _network = utiles.isNetworkAvailable(getApplicationContext());
                if(_network.equals("1"))
                    Upadate_InformationPersonne();
                else
                    Toast.makeText(getApplicationContext(), "Échec ,veuillez vérifier votre connexion internet", Toast.LENGTH_LONG).show();

            }
        });

        AffectationInformationProfil();
    }
    public void AffectationInformationProfil()
    {
        if(Personne._PERSONNE.get(0).get_image() != null)
            imageView_imageProfil.setImageBitmap(Personne._PERSONNE.get(0).get_image());
        editText_nom.setText(Personne._PERSONNE.get(0).get_nom());
        editText_prenom.setText(Personne._PERSONNE.get(0).get_prenom());
        editText_password.setText(Personne._PERSONNE.get(0).get_pass());
        editText_repetezpassword.setText(Personne._PERSONNE.get(0).get_pass());
        editText_email.setText(Personne._PERSONNE.get(0).get_email());
        editText_numerocarteidentite.setText(Personne._PERSONNE.get(0).get_cin());
        editText_numerotelphone.setText(Personne._PERSONNE.get(0).get_num_telephone());
        editText_adresse.setText(Personne._PERSONNE.get(0).get_adresse());

    }

    private void Upadate_InformationPersonne()
    {
        final String _idPersonne = Personne._PERSONNE.get(0).get_id_personne();
        final String _nom = editText_nom.getText().toString().trim();
        final String _prenom = editText_prenom.getText().toString().trim();
        final String _password = editText_password.getText().toString().trim();
        final String _email = editText_email.getText().toString().trim();
        final String _numeroCarteIdentite = editText_numerocarteidentite.getText().toString().trim();
        final String _numeroTelephone = editText_numerotelphone.getText().toString().trim();
        final String _adresse = editText_adresse.getText().toString().trim();

        class UpdatePersonne extends AsyncTask<Void,Void,String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Activity_Profil.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                loading.dismiss();
                function_resultRequete(s);

            }

            @Override
            protected String doInBackground(Void... v)
            {
                HashMap<String,String> params = new HashMap<>();
                Bitmap _imageBitmapProfil = null ;

                if(_imageProfilString.equals(""))// si l'utilisateur n'a pas choisit une image
                {
                    if (Personne._PERSONNE.get(0).get_image() == null)// si l'utilisateur n'a j'amis eu d'image
                    {
                        _imageBitmapProfil = null;
                        params.put(Configuration.TAG_JSON_PERSONNE_image, "");
                    } else
                    { // si l'utilisateur a une encienne image
                        _imageBitmapProfil = Personne._PERSONNE.get(0).get_image();
                        String _imageString = Utiles.BitmapToString(Personne._PERSONNE.get(0).get_image());
                        params.put(Configuration.TAG_JSON_PERSONNE_image,_imageString);
                    }
                }
                else // si l'utilisateur a choisit une image
                {
                    _imageBitmapProfil = Utiles.StringToBitMap(_imageProfilString);
                    params.put(Configuration.TAG_JSON_PERSONNE_image,_imageProfilString);
                }

                params.put(Configuration.TAG_JSON_PERSONNE_idPersonne,_idPersonne);
                params.put(Configuration.TAG_JSON_PERSONNE_nom,_nom);
                params.put(Configuration.TAG_JSON_PERSONNE_prenom,_prenom);
                params.put(Configuration.TAG_JSON_PERSONNE_pass,_password);
                params.put(Configuration.TAG_JSON_PERSONNE_email,_email);
                params.put(Configuration.TAG_JSON_PERSONNE_cin,_numeroCarteIdentite);
                params.put(Configuration.TAG_JSON_PERSONNE_numTelephone,_numeroTelephone);
                params.put(Configuration.TAG_JSON_PERSONNE_adresse,_adresse);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest_Add_Update(Configuration.URL_UPDATE_PERSONNE, params);
                if(res.equals("1"))
                {
                    Affectation_UpdatePersonne(_idPersonne,_nom,_prenom,_numeroCarteIdentite,_numeroTelephone,_imageBitmapProfil,_email,_adresse,_password);
                }
                return res;
            }
        }

        UpdatePersonne ae = new UpdatePersonne();
        ae.execute();
    }

    void Affectation_UpdatePersonne(String _id_personne,String _nom,String _prenom,String _cin,String _numTelephone , Bitmap _bitmapImage,String _email , String _adresse,String _pass)
    {
        Personne _personne = new Personne(_id_personne,_nom,_prenom,_cin,_numTelephone,_bitmapImage,_email,_adresse,_pass);

        Personne._PERSONNE.clear();
        Personne._PERSONNE.add(_personne);
    }
    void function_resultRequete(String _ok)
    {
        if (_ok.equals("1"))
        {
            Toast.makeText(getApplicationContext(), "mise à jour avec succès", Toast.LENGTH_LONG).show();
            /*Intent startactivity_NavigationDrawer = new Intent(getActivity(), Activity_NavigationDrawer.class);
            startActivity(startactivity_NavigationDrawer);
            getActivity().finish();*/

        } else {
            Toast.makeText(getApplicationContext(), "Échec ,Veuillez vérifier vos informations ", Toast.LENGTH_LONG).show();
        }
    }

    public void choisirImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (data != null && data.getData() != null)
        {
            Uri filePath = data.getData();
            try {
                Bitmap _imagechoisit= MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                _imageProfilString = Utiles.BitmapToString(_imagechoisit);
                //Log.d("xxx", "onActivityResult: image_gallery_1:"+_imageProfilString);
                Bitmap _iamgeBitmap = Utiles.StringToBitMap(_imageProfilString);
                imageView_imageProfil.setImageBitmap(_iamgeBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
