package com.example.ramzi.myapplication.Package_utile;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.ramzi.myapplication.Package_Classe.Produit;
import com.example.ramzi.myapplication.Package_Navdrawer.Activity_NavigationDrawer;
import com.example.ramzi.myapplication.R;

import java.util.ArrayList;

/**
 * Created by ramzi on 22/02/16.
 */
public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder>
{
    ArrayList<Produit> _ArrayList_Produit;
    //ArrayList<Integer> alImage;
    Context context;

    public GridAdapter(Context context, ArrayList<Produit> _ArrayList_Produit)
    {
        super();
        this.context = context;
        this._ArrayList_Produit = _ArrayList_Produit;
        //this.alImage = alImage;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        viewHolder.textview_libelle.setText(_ArrayList_Produit.get(i).get_libelle());
        //viewHolder.imageview_produit.setImageResource(_ArrayList_Produit.get(i).get_image());

        viewHolder.setClickListener(new ItemClickListener()
        {
            public void onClick(View view, int position, boolean isLongClick)
            {
                if (isLongClick)
                {
                    Toast.makeText(context, "#" + position + " - " + _ArrayList_Produit.get(position).get_libelle() + " (Long click)", Toast.LENGTH_SHORT).show();
                    //context.startActivity(new Intent(context, Activity_NavigationDrawer.class));
                } else {
                    Toast.makeText(context, "#" + position + " - " + _ArrayList_Produit.get(position).get_libelle(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return _ArrayList_Produit.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public ImageView imageview_produit;
        public TextView textview_libelle;
        private ItemClickListener clickListener;

        public ViewHolder(View itemView)
        {
            super(itemView);
            imageview_produit = (ImageView) itemView.findViewById(R.id.cardviewitem_imageview_produit);
            textview_libelle = (TextView) itemView.findViewById(R.id.cardviewitem_textview_libelle);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getPosition(), true);
            return true;
        }
    }

}