package com.example.ramzi.myapplication.Package_Navdrawer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ramzi.myapplication.Package_Classe.Categorie;
import com.example.ramzi.myapplication.Package_Classe.Personne;
import com.example.ramzi.myapplication.Package_Classe.Produit;
import com.example.ramzi.myapplication.Package_Navdrawer.Package_MesAnnonces.Activity_MesAnnonces;
import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.Package_TabsConnexionInscription.Activity_Tabs;
import com.example.ramzi.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Activity_NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public  ImageView _ImageView_imageProfil;
    public  TextView _TextView_nomPrenom;
    public  TextView _TextView_email;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navdrawer_activity);
         /**/
        Select_ProduiCategorie();
        /**/
        toolbar = (Toolbar) findViewById(R.id.navdrawer_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.navdrawer_tabs_pager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.navdrawer_tabs_name);
        tabLayout.setupWithViewPager(viewPager);
        /**/
        FloatingActionButton _Buttton_AjouterAnnonce = (FloatingActionButton) findViewById(R.id.bfs_ajouterannonce);
        _Buttton_AjouterAnnonce.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                Snackbar.make(view, "Ajouter une annonce", Snackbar.LENGTH_LONG).setAction("confirmer", new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent _start_activityAjouterAnnonce = new Intent(Activity_NavigationDrawer.this,Activity_AjouterAnnonce.class);
                        startActivity(_start_activityAjouterAnnonce);
                    }
                }).show();
            }
        });
        /**/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        {
            public void onDrawerClosed(View view)
            {
                //getActionBar().setTitle(mTitle);
                //Toast.makeText(getApplicationContext(),"Closed",Toast.LENGTH_LONG).show();
                AffectationInformationProfil();
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView)
            {
                //getActionBar().setTitle(mDrawerTitle);
                //Toast.makeText(getApplicationContext(),"Opened",Toast.LENGTH_LONG).show();
                //AffectationInformationProfil();
                invalidateOptionsMenu();
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        /**/
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView =  navigationView.getHeaderView(0);
        _ImageView_imageProfil = (ImageView) hView.findViewById(R.id.navdrawer_imageView_imageProfil);
        _TextView_nomPrenom = (TextView) hView.findViewById(R.id.navdrawer_textview_nomprenom);
        _TextView_email = (TextView) hView.findViewById(R.id.navdrawer_textview_email);

        AffectationInformationProfil();
    }

    public void AffectationInformationProfil()
    {
        //Log.d("xxx", "AffectationInformationProfil: "+Personne._PERSONNE.get(0).get_nom()+" "+Personne._PERSONNE.get(0).get_prenom()+" "+Personne._PERSONNE.get(0).get_email());
        if(Personne._PERSONNE.get(0).get_image() != null)
            _ImageView_imageProfil.setImageBitmap(Personne._PERSONNE.get(0).get_image());
        _TextView_nomPrenom.setText(Personne._PERSONNE.get(0).get_nom()+" "+Personne._PERSONNE.get(0).get_prenom());
        _TextView_email.setText(Personne._PERSONNE.get(0).get_email());
    }

    /***************/
    private void Select_ProduiCategorie()
    {
        class GetEmployee extends AsyncTask<Void,Void,String>
        {
            ProgressDialog loading;
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(Activity_NavigationDrawer.this,"Fetching...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                loading.dismiss();
            }

            @Override
            protected String doInBackground(Void... params)
            {
                RequestHandler rh = new RequestHandler();
                String s_produit = rh.sendGetRequest(Configuration.URL_SELECT_PRODUIT);
                Log.d("xxx_produit", "onPostExecute:  Produit:"+s_produit);
                Produit._List_PRODUIT_jeuxvideos.clear();
                Produit._List_PRODUIT_ordinateur.clear();
                Produit._List_PRODUIT_telephonie.clear();
                Affectation_JsonListProduit(s_produit);
                /**/
                String s_categorie = rh.sendGetRequest(Configuration.URL_SELECT_CATEGORIE);
                Log.d("xxx_categorie", "onPostExecute:  categorie:"+s_categorie);
                Categorie._List_CATEGORIE.clear();
                Affectation_JsonListCategorie(s_categorie);
                return null;
            }
        }
        GetEmployee ge = new GetEmployee();
        ge.execute();
    }

    private void Affectation_JsonListCategorie(String json)
    {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            for (int i=0 ;i<result.length() ;i++)
            {
                JSONObject c = result.getJSONObject(i);

                String _id_categorie = c.getString(Configuration.TAG_JSON_CATEGORIE_idCategore);
                String _nom = c.getString(Configuration.TAG_JSON_CATEGORIE_nom);

                Categorie _categorie = new Categorie(_id_categorie,_nom);

                Categorie._List_CATEGORIE.add(_categorie);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Affectation_JsonListProduit(String json)
    {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            for (int i=0 ;i<result.length() ;i++)
            {
                JSONObject c = result.getJSONObject(i);

                String _id_produit = c.getString(Configuration.TAG_JSON_PRODUIT_idProduit);
                String _libelle = c.getString(Configuration.TAG_JSON_PRODUIT_libelle);
                String _prix = c.getString(Configuration.TAG_JSON_PRODUIT_prix);
                String _tempsExpiration = c.getString(Configuration.TAG_JSON_PRODUIT_tempsExpiration);
                String _image = c.getString(Configuration.TAG_JSON_PRODUIT_image);
                String _caracteristique = c.getString(Configuration.TAG_JSON_PRODUIT_caracteristique);
                String _id_categorie = c.getString(Configuration.TAG_JSON_PRODUIT_idCategorie);
                String _id_personne = c.getString(Configuration.TAG_JSON_PRODUIT_idPersonne);
                String _valider = c.getString(Configuration.TAG_JSON_PRODUIT_valider);
                String _date_validation = c.getString(Configuration.TAG_JSON_PRODUIT_dateValidation);

                Produit _Produit = new Produit(_id_produit,_libelle,_prix,_tempsExpiration,_image,_caracteristique,_id_categorie,_id_personne, _valider, _date_validation);

                if(_id_categorie.equals("1"))//ordinateur
                {
                    Produit._List_PRODUIT_ordinateur.add(_Produit);
                }if(_id_categorie.equals("2"))//Téléphonie
                {
                    Produit._List_PRODUIT_telephonie.add(_Produit);
                }if(_id_categorie.equals("3"))//jeux vidéo
                {
                    Produit._List_PRODUIT_jeuxvideos.add(_Produit);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /************************/


    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Navdrawer_FragmentJeuVideo(), "Jeu Vidéo");
        adapter.addFragment(new Navdrawer_FragmentOrdinateur(), "Ordinateur");
        adapter.addFragment(new Navdrawer_FragmentTelephonie(), "Téléphonie");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }
    // this method is used to create list of items.

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_deconnexion)
        {
            Intent startactivity_NavigationDrawer = new Intent(this , Activity_Tabs.class);
            startActivity(startactivity_NavigationDrawer);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_profil)
        {
            Intent _activity_profil = new Intent(this,Activity_Profil.class);
            startActivity(_activity_profil);
        } else if (id == R.id.nav_mes_annonce)
        {
            Intent _activity_annonces = new Intent(this,Activity_MesAnnonces.class);
            startActivity(_activity_annonces);
        } else if (id == R.id.nav_panier)
        {
            Intent _activity_panier = new Intent(this,Activity_Panier.class);
            startActivity(_activity_panier);
        } else if (id == R.id.nav_livraison)
        {

        } else if (id == R.id.nav_message)
        {

        } else if (id == R.id.nav_reclamation)
        {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
