package com.example.ramzi.myapplication.Package_Navdrawer.Package_MesAnnonces;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.example.ramzi.myapplication.Package_Classe.Personne;
import com.example.ramzi.myapplication.Package_Classe.Produit;
import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Activity_MesAnnonces extends AppCompatActivity
{
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesannonces);

        Select_MesProduit();

        toolbar = (Toolbar) findViewById(R.id.navdrawer_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



    }


    private void Select_MesProduit()
    {
        class GetEmployee extends AsyncTask<Void,Void,String>
        {
            ProgressDialog loading;
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(Activity_MesAnnonces.this,"Fetching...","Wait...",false,false);

            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                Log.d("xxx", "onPostExecute:  "+s);
                loading.dismiss();
                Produit._List_MES_PRODUIT.clear();
                AffectationJson_MesProduit(Produit._List_MES_PRODUIT,s);
            }

            @Override
            protected String doInBackground(Void... params)
            {
                String s = null;
                try
                {
                    RequestHandler rh = new RequestHandler();
                    String _id_personne = Personne._PERSONNE.get(0).get_id_personne();
                    s = rh.sendGetRequestParam_MesProduit(Configuration.URL_SELECT_MESPRODUIT, URLEncoder.encode(_id_personne, "utf-8"));

                } catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                }
                return s;
            }
        }
        GetEmployee ge = new GetEmployee();
        ge.execute();
    }
    private void AffectationJson_MesProduit(ArrayList<Produit> _MesProduit, String json)
    {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            for (int i=0 ;i<result.length() ;i++)
            {
                JSONObject c = result.getJSONObject(i);

                String _id_produit = c.getString(Configuration.TAG_JSON_PRODUIT_idProduit);
                String _libelle = c.getString(Configuration.TAG_JSON_PRODUIT_libelle);
                String _prix = c.getString(Configuration.TAG_JSON_PRODUIT_prix);
                String _tempsExpiration = c.getString(Configuration.TAG_JSON_PRODUIT_tempsExpiration);
                String _image = c.getString(Configuration.TAG_JSON_PRODUIT_image);
                String _caracteristique = c.getString(Configuration.TAG_JSON_PRODUIT_caracteristique);
                String _id_categorie = c.getString(Configuration.TAG_JSON_PRODUIT_idCategorie);
                String _id_personne = c.getString(Configuration.TAG_JSON_PRODUIT_idPersonne);
                String _valider = c.getString(Configuration.TAG_JSON_PRODUIT_valider);
                String _date_validation = c.getString(Configuration.TAG_JSON_PRODUIT_dateValidation);

                Produit _mesproduit = new Produit(_id_produit,_libelle,_prix,_tempsExpiration,_image,_caracteristique,_id_categorie,_id_personne, _valider, _date_validation);
                _MesProduit.add(_mesproduit);
            }

            if(result.length()==0)
                function_resultRequete(false);
            else
                function_resultRequete(true);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    void function_resultRequete(boolean _ok)
    {
        if (_ok)
        {
            //Toast.makeText(Activity_MesAnnonces.this, "", Toast.LENGTH_LONG).show();
            /*Intent startactivity_NavigationDrawer = new Intent(getActivity(), Activity_NavigationDrawer.class);
            startActivity(startactivity_NavigationDrawer);
            getActivity().finish();*/

        } else {
            Toast.makeText(Activity_MesAnnonces.this, "vous n'avez aucune annonce", Toast.LENGTH_LONG).show();
        }
    }

}
