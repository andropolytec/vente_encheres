package com.example.ramzi.myapplication.Package_Classe;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by ramzi on 27/03/16.
 */
public class Personne
{
    public static ArrayList<Personne> _PERSONNE = new ArrayList<Personne>();

    private String _id_personne;
    private String _nom;
    private String _prenom;
    private String _cin;
    private String _num_telephone;
    private Bitmap _image;
    private String _email;
    private String _adresse;
    private String _pass;

    public Personne(String _id_personne, String _nom, String _prenom, String _cin, String _num_telephone, Bitmap _image, String _email, String _adresse,String _pass) {
        this._id_personne = _id_personne;
        this._nom = _nom;
        this._prenom = _prenom;
        this._cin = _cin;
        this._num_telephone = _num_telephone;
        this._image = _image;
        this._email = _email;
        this._adresse = _adresse;
        this._pass = _pass;
    }

    public String get_pass() {
        return _pass;
    }

    public void set_pass(String _pass) {
        this._pass = _pass;
    }

    public String get_id_personne() {
        return _id_personne;
    }

    public String get_nom() {
        return _nom;
    }

    public String get_prenom() {
        return _prenom;
    }

    public String get_cin() {
        return _cin;
    }

    public String get_num_telephone() {
        return _num_telephone;
    }

    public Bitmap get_image() {
        return _image;
    }

    public String get_email() {
        return _email;
    }

    public String get_adresse() {
        return _adresse;
    }

    public void set_id_personne(String  _id_personne) {
        this._id_personne = _id_personne;
    }

    public void set_nom(String _nom) {
        this._nom = _nom;
    }

    public void set_prenom(String _prenom) {
        this._prenom = _prenom;
    }

    public void set_cin(String _cin) {
        this._cin = _cin;
    }

    public void set_num_telephone(String _num_telephone) {
        this._num_telephone = _num_telephone;
    }

    public void set_image(Bitmap _image) {
        this._image = _image;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public void set_adresse(String _adresse) {
        this._adresse = _adresse;
    }
}
