package com.example.ramzi.myapplication.Package_TabsConnexionInscription;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ramzi.myapplication.Package_Classe.Personne;
import com.example.ramzi.myapplication.Package_Classe.Produit;
import com.example.ramzi.myapplication.Package_Navdrawer.Activity_NavigationDrawer;
import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.Package_utile.Utiles;
import com.example.ramzi.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by ramzi on 23/03/16.
 */
public class Tabs_FragmentConnexion extends Fragment
{
    EditText _editText_email ;
    EditText _editText_password ;
    ImageView _imageProfil;

    public Tabs_FragmentConnexion()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.tabs_fragmentconnexion, container, false);

        _imageProfil = (ImageView) view.findViewById(R.id.fragmentconnexion_imageview_imageprofil);
        _editText_email = (EditText) view.findViewById(R.id.fragmentconnexion_edittext_email);
        _editText_password = (EditText) view.findViewById(R.id.fragmentconnexion_edittext_password);

        Button _button_connexion = (Button) view.findViewById(R.id.fragmentconnexion_button_connexion);

        _button_connexion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Utiles utiles = new Utiles();
                String _network = utiles.isNetworkAvailable(getActivity());
                if(_network.equals("1"))
                    Connexion_Personne(_editText_email.getText().toString().trim(),_editText_password.getText().toString().trim());
                else
                {
                    Toast.makeText(getActivity(), "Échec ,veuillez vérifier votre connexion internet", Toast.LENGTH_LONG).show();
                }

            }
        });
        return view;
    }

    private void Connexion_Personne(final String _email , final String _password)
    {
        class GetEmployee extends AsyncTask<Void,Void,String>
        {
            ProgressDialog loading;
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Fetching...","Wait...",false,false);

            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                Log.d("xxx", "onPostExecute:  "+s);
                loading.dismiss();
               AffectationJson_ConnexionPersonne(Personne._PERSONNE,s);
            }

            @Override
            protected String doInBackground(Void... params)
            {
                String s = null;
                try
                {
                    RequestHandler rh = new RequestHandler();
                    s = rh.sendGetRequestParam_ConnexionPersonne(Configuration.URL_CONNEXION_PERSONNE,
                            URLEncoder.encode(_email, "utf-8"),URLEncoder.encode(_password, "utf-8"));

                } catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                }
                return s;
            }
        }
        GetEmployee ge = new GetEmployee();
        ge.execute();
    }
    private void AffectationJson_ConnexionPersonne(ArrayList<Personne> _Personne, String json)
    {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(Configuration.TAG_JSON_ARRAY);

            for (int i=0 ;i<result.length() ;i++)
            {
                JSONObject c = result.getJSONObject(i);

                String _id_personne = c.getString(Configuration.TAG_JSON_PERSONNE_idPersonne);
                String _nom =  c.getString(Configuration.TAG_JSON_PERSONNE_nom);
                String _prenom =  c.getString(Configuration.TAG_JSON_PERSONNE_prenom);
                String _cin =  c.getString(Configuration.TAG_JSON_PERSONNE_cin);
                String _numTelephone =  c.getString(Configuration.TAG_JSON_PERSONNE_numTelephone);
                String _image = c.getString(Configuration.TAG_JSON_PERSONNE_image);
                String _email =  c.getString(Configuration.TAG_JSON_PERSONNE_email);
                String _adresse =  c.getString(Configuration.TAG_JSON_PERSONNE_adresse);
                String _pass=  c.getString(Configuration.TAG_JSON_PERSONNE_pass);

                Log.d("xxx", "AffectationJson_ConnexionPersonne: image_BD:"+_image);
                Bitmap _bitmapImage = null;
                if(_image.equals("")==false)
                {
                    _bitmapImage = Utiles.StringToBitMap(_image);
                    _imageProfil.setImageBitmap(_bitmapImage);
                }


                Personne _personne = new Personne(_id_personne,_nom,_prenom,_cin,_numTelephone,_bitmapImage,_email,_adresse,_pass);

                _Personne.clear();
               _Personne.add(_personne);

            }

            if(result.length()==0)
                function_resultRequete(false);
            else
                function_resultRequete(true);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    void function_resultRequete(boolean _ok)
    {
        if (_ok)
        {
            Toast.makeText(getActivity(), "connecté avec succès", Toast.LENGTH_LONG).show();
            Intent startactivity_NavigationDrawer = new Intent(getActivity(), Activity_NavigationDrawer.class);
            startActivity(startactivity_NavigationDrawer);
            getActivity().finish();

        } else {
            Toast.makeText(getActivity(), "Échec ,Veuillez vérifier vos informations ", Toast.LENGTH_LONG).show();
        }
    }
}
