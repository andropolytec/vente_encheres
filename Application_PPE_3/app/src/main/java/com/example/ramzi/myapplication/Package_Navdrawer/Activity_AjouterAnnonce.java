package com.example.ramzi.myapplication.Package_Navdrawer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ramzi.myapplication.Package_Classe.Categorie;
import com.example.ramzi.myapplication.Package_Classe.Personne;
import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.Package_TabsConnexionInscription.Activity_Tabs;
import com.example.ramzi.myapplication.Package_utile.Utiles;
import com.example.ramzi.myapplication.R;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Activity_AjouterAnnonce extends AppCompatActivity
{
    private String _imageStringProduit = "";
    private String _id_categorie = "3"; // jeux vidéo

    private ImageView _imageProduit;
    private EditText _libelle;
    private EditText _caracteristique;
    private EditText _prix;
    private Spinner _spinnerCategorie;
    private Button _buttonAjouter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouterannonce);

        String[] spinner_items = new String[3];
        int i=0;
        for (i=0 ; i < Categorie._List_CATEGORIE.size() ; i++)
        {
            spinner_items[i] = Categorie._List_CATEGORIE.get(i).get_nom();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.navdrawer_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _imageProduit = (ImageView) findViewById(R.id.activityajouterannonce_imageview_image);
        _libelle = (EditText) findViewById(R.id.activityajouterannonce_edittext_libelle);
        _caracteristique = (EditText) findViewById(R.id.activityajouterannonce_edittext_caracteristique);
        _prix = (EditText) findViewById(R.id.activityajouterannonce_edittext_prix);
        _buttonAjouter = (Button) findViewById(R.id.activityajouterannonce_button_ajouter);
        _spinnerCategorie = (Spinner) findViewById(R.id.activityajouterannonce_spinner_categorie);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinner_items);
        _spinnerCategorie.setAdapter(adapter);
        _spinnerCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String _item =  (String) parent.getItemAtPosition(position);
                for (int i=0 ; i<Categorie._List_CATEGORIE.size() ; i++)
                {
                    if(_item.equalsIgnoreCase(Categorie._List_CATEGORIE.get(i).get_nom()))
                        _id_categorie = Categorie._List_CATEGORIE.get(i).get_id_categorie();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                // TODO Auto-generated method stub
            }
        });
        _imageProduit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choisirImage();
            }
        });
        _buttonAjouter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Ajouter_Prosuit();
            }
        });
    }

    private String calcul_TexpsExpiration(int _add_jour)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, _add_jour); // Adding days
        String _day_add= sdf.format(c.getTime());
        return _day_add;
    }

    private void Ajouter_Prosuit()
    {
        final String libelle = _libelle.getText().toString().trim();
        final String prix = _prix.getText().toString().trim();
        final String tempsExpiration = calcul_TexpsExpiration(61);
        final String caracteristique = _caracteristique.getText().toString().trim();
        final String _id_personne = Personne._PERSONNE.get(0).get_id_personne();

        class AddPersonne extends AsyncTask<Void,Void,String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Activity_AjouterAnnonce.this,"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                loading.dismiss();
                function_resultRequete(s);

            }

            @Override
            protected String doInBackground(Void... v)
            {
                HashMap<String,String> params = new HashMap<>();

                params.put(Configuration.TAG_JSON_PRODUIT_libelle,libelle);
                params.put(Configuration.TAG_JSON_PRODUIT_prix,prix);
                params.put(Configuration.TAG_JSON_PRODUIT_tempsExpiration,tempsExpiration);
                params.put(Configuration.TAG_JSON_PRODUIT_image,_imageStringProduit);
                params.put(Configuration.TAG_JSON_PRODUIT_caracteristique,caracteristique);
                params.put(Configuration.TAG_JSON_PRODUIT_idCategorie,_id_categorie);
                params.put(Configuration.TAG_JSON_PRODUIT_idPersonne,_id_personne);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest_Add_Update(Configuration.URL_ADD_PRODUIT, params);
                return res;
            }
        }

        AddPersonne ae = new AddPersonne();
        ae.execute();
    }

    void function_resultRequete(String _ok)
    {
        if (_ok.equals("1"))
        {
            Toast.makeText(Activity_AjouterAnnonce.this, "Ajout avec succès", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(Activity_AjouterAnnonce.this, "Échec ,Veuillez vérifier vos informations ", Toast.LENGTH_LONG).show();
        }
    }

    public void choisirImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (data != null && data.getData() != null)
        {
            Uri filePath = data.getData();
            try {
                Bitmap _image = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                _imageStringProduit = Utiles.BitmapToString(_image);
                Log.d("xxx", "onActivityResult: image_gallery_1:"+_imageStringProduit);
                Bitmap _iamgeBitmap = Utiles.StringToBitMap(_imageStringProduit);
                _imageProduit.setImageBitmap(_iamgeBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
