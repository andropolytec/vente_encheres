package com.example.ramzi.myapplication.Package_utile;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view, int position, boolean isLongClick);
}
