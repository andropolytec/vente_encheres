package com.example.ramzi.myapplication.Package_Navdrawer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.example.ramzi.myapplication.Package_Classe.Produit;
import com.example.ramzi.myapplication.Package_utile.GridAdapter;
import com.example.ramzi.myapplication.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ramzi on 24/03/16.
 */
public class Navdrawer_FragmentJeuVideo extends Fragment
{
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;

    public SearchView search;

    public Navdrawer_FragmentJeuVideo()
    {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.navdrawer_fragmentjeuvideo, container, false);

        //createlist();  // in this method, Create a list of items.
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        search = (SearchView) view.findViewById( R.id.search);

        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GridAdapter(getActivity(), Produit._List_PRODUIT_jeuxvideos);
        mRecyclerView.setAdapter(mAdapter);

        search.setOnQueryTextListener(listener); // call the QuerytextListner.

        return view;
    }

    /*public void createlist()
    {
        alName = new ArrayList<>(Arrays.asList("Cheesy...", "Crispy... ", "Fizzy...", "Cool...", "Softy...", "Fruity...", "Fresh...", "Sticky..."));
        alImage = new ArrayList<>(Arrays.asList(R.drawable.one, R.drawable.four, R.drawable.four,
                R.drawable.four, R.drawable.four, R.drawable.image, R.drawable.four, R.drawable.four));

    }*/

    /***********/
    SearchView.OnQueryTextListener listener = new SearchView.OnQueryTextListener()
    {
        @Override
        public boolean onQueryTextChange(String query)
        {
            query = query.toLowerCase();

            final ArrayList<Produit> filtered_ArrayListProduit_jeuxvideos = new ArrayList<Produit>();
            //final ArrayList<Integer> filtered_image = new ArrayList<>();

            for (int i = 0; i < Produit._List_PRODUIT_jeuxvideos.size(); i++)
            {

                final String text = Produit._List_PRODUIT_jeuxvideos.get(i).get_libelle().toLowerCase();
                if (text.contains(query))
                {

                    filtered_ArrayListProduit_jeuxvideos.add(Produit._List_PRODUIT_jeuxvideos.get(i));
                    //filtered_image.add(alImage.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new GridAdapter(getActivity(), filtered_ArrayListProduit_jeuxvideos);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };
}
