package com.example.ramzi.myapplication.Package_TabsConnexionInscription;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ramzi.myapplication.Package_Navdrawer.Activity_NavigationDrawer;
import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.Package_utile.Utiles;
import com.example.ramzi.myapplication.R;

import java.util.HashMap;

/**
 * Created by ramzi on 23/03/16.
 */
public class Tabs_FragmentInscription extends Fragment
{
    ImageView imageView_image;
    EditText editText_nom;
    EditText editText_prenom;
    EditText editText_password;
    EditText editText_repetezpassword ;
    EditText editText_email ;
    EditText editText_numerocarteidentite ;
    EditText editText_numerotelphone ;
    EditText editText_adresse ;

    public Tabs_FragmentInscription()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.tabs_fragmentinscription, container, false);

        imageView_image = (ImageView) view.findViewById(R.id.fragmentinscription_imageview_image);
        editText_nom = (EditText) view.findViewById(R.id.fragmentinscription_edittext_name);
        editText_prenom = (EditText) view.findViewById(R.id.fragmentinscription_edittext_prenom);
        editText_password = (EditText) view.findViewById(R.id.fragmentinscription_edittext_password);
        editText_repetezpassword = (EditText) view.findViewById(R.id.fragmentinscription_edittext_repetezpassword);
        editText_email = (EditText) view.findViewById(R.id.fragmentinscription_edittext_email);
        editText_numerocarteidentite = (EditText) view.findViewById(R.id.fragmentinscription_edittext_numcarteidentite);
        editText_numerotelphone = (EditText) view.findViewById(R.id.fragmentinscription_edittext_numerotelephoen);
        editText_adresse = (EditText) view.findViewById(R.id.fragmentinscription_edittext_adresse);
        Button button_inscription = (Button) view.findViewById(R.id.fragmentinscription_button_inscription);


        imageView_image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                choisirImage();
            }
        });

        button_inscription.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Utiles utiles = new Utiles();
                String _network = utiles.isNetworkAvailable(getActivity());
                if(_network.equals("1"))
                    Inscription_Personne();
                else
                    Toast.makeText(getActivity(), "Échec ,veuillez vérifier votre connexion internet", Toast.LENGTH_LONG).show();

            }
        });

        return view;
    }

    public void choisirImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void Inscription_Personne()
    {
        Log.d("xxx", "add_Personne: image_gallery_2:"+Activity_Tabs._imageProfilString);
        final String _nom = editText_nom.getText().toString().trim();
        final String _prenom = editText_prenom.getText().toString().trim();
        final String _password = editText_password.getText().toString().trim();
        final String _email = editText_email.getText().toString().trim();
        final String _numeroCarteIdentite = editText_numerocarteidentite.getText().toString().trim();
        final String _numeroTelephone = editText_numerotelphone.getText().toString().trim();
        final String _adresse = editText_adresse.getText().toString().trim();

        class AddPersonne extends AsyncTask<Void,Void,String>{

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Adding...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                loading.dismiss();
                function_resultRequete(s);

            }

            @Override
            protected String doInBackground(Void... v)
            {
                HashMap<String,String> params = new HashMap<>();

                if(Activity_Tabs._imageProfilString.equals(""))
                    params.put(Configuration.TAG_JSON_PERSONNE_image,"");
                else
                    params.put(Configuration.TAG_JSON_PERSONNE_image,Activity_Tabs._imageProfilString);

                params.put(Configuration.TAG_JSON_PERSONNE_nom,_nom);
                params.put(Configuration.TAG_JSON_PERSONNE_prenom,_prenom);
                params.put(Configuration.TAG_JSON_PERSONNE_pass,_password);
                params.put(Configuration.TAG_JSON_PERSONNE_email,_email);
                params.put(Configuration.TAG_JSON_PERSONNE_cin,_numeroCarteIdentite);
                params.put(Configuration.TAG_JSON_PERSONNE_numTelephone,_numeroTelephone);
                params.put(Configuration.TAG_JSON_PERSONNE_adresse,_adresse);


                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest_Add_Update(Configuration.URL_ADD_PERSONNE, params);
                return res;
            }
        }

        AddPersonne ae = new AddPersonne();
        ae.execute();
    }

    void function_resultRequete(String _ok)
    {
        if (_ok.equals("1"))
        {
            Toast.makeText(getActivity(), "inscription avec succès", Toast.LENGTH_LONG).show();
            /*Intent startactivity_NavigationDrawer = new Intent(getActivity(), Activity_NavigationDrawer.class);
            startActivity(startactivity_NavigationDrawer);
            getActivity().finish();*/

        } else {
            Toast.makeText(getActivity(), "Échec ,Veuillez vérifier vos informations ", Toast.LENGTH_LONG).show();
        }
    }

}
