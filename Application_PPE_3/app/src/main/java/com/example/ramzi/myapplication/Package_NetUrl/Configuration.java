package com.example.ramzi.myapplication.Package_NetUrl;

/**
 * Created by ramzi on 26/03/16.
 */
public class Configuration
{
    public static final String IP_SERVER = "192.168.43.211";

    public static final String URL_SELECT_MESPRODUIT = "http://"+IP_SERVER+"/RamziProject/PPE_android/select_MesProduit.php";
    public static final String URL_SELECT_PRODUIT = "http://"+IP_SERVER+"/RamziProject/PPE_android/select_Produit.php";
    public static final String URL_ADD_PRODUIT = "http://"+IP_SERVER+"/RamziProject/PPE_android/add_Produit.php";

    public static final String URL_SELECT_CATEGORIE = "http://"+IP_SERVER+"/RamziProject/PPE_android/select_Categorie.php";

    public static final String URL_ADD_PERSONNE = "http://"+IP_SERVER+"/RamziProject/PPE_android/add_Personne.php";
    public static final String URL_UPDATE_PERSONNE = "http://"+IP_SERVER+"/RamziProject/PPE_android/update_Personne.php";
    public static final String URL_CONNEXION_PERSONNE = "http://"+IP_SERVER+"/RamziProject/PPE_android/connexion_Personne.php";

    // JSON Tags
    public static final String TAG_JSON_ARRAY="result";

    public static final String TAG_JSON_PRODUIT_idProduit="id_produit";
    public static final String TAG_JSON_PRODUIT_libelle="libelle";
    public static final String TAG_JSON_PRODUIT_prix="prix";
    public static final String TAG_JSON_PRODUIT_tempsExpiration="tempsExpiration";
    public static final String TAG_JSON_PRODUIT_image="image";
    public static final String TAG_JSON_PRODUIT_caracteristique="caracteristique";
    public static final String TAG_JSON_PRODUIT_idCategorie="id_categorie";
    public static final String TAG_JSON_PRODUIT_idPersonne="id_personne";
    public static final String TAG_JSON_PRODUIT_valider="valider";
    public static final String TAG_JSON_PRODUIT_dateValidation="date_validation";

    // JSON Tags
    public static final String TAG_JSON_PERSONNE_idPersonne="id_personne";
    public static final String TAG_JSON_PERSONNE_nom="nom";
    public static final String TAG_JSON_PERSONNE_prenom="prenom";
    public static final String TAG_JSON_PERSONNE_cin="cin";
    public static final String TAG_JSON_PERSONNE_numTelephone="num_telephone";
    public static final String TAG_JSON_PERSONNE_image="image";
    public static final String TAG_JSON_PERSONNE_email="email";
    public static final String TAG_JSON_PERSONNE_adresse="adresse";
    public static final String TAG_JSON_PERSONNE_pass="pass";

    // JSON Tags
    public static final String TAG_JSON_CATEGORIE_idCategore="id_categorie";
    public static final String TAG_JSON_CATEGORIE_nom="nom";



}
