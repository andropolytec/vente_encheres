package com.example.ramzi.myapplication.Package_TabsConnexionInscription;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ramzi.myapplication.Package_NetUrl.Configuration;
import com.example.ramzi.myapplication.Package_NetUrl.RequestHandler;
import com.example.ramzi.myapplication.Package_utile.Utiles;
import com.example.ramzi.myapplication.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Activity_Tabs extends AppCompatActivity
{
    public static String _imageProfilString = "";
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.tabs_pager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs_name);
        tabLayout.setupWithViewPager(viewPager);
    }




   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (data != null && data.getData() != null)
        {
            Uri filePath = data.getData();
            try {
                Bitmap _imageProfil = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                _imageProfilString = Utiles.BitmapToString(_imageProfil);
                Log.d("xxx", "onActivityResult: image_gallery_1:"+_imageProfilString);
                Bitmap _iamgeBitmap = Utiles.StringToBitMap(_imageProfilString);
                ImageView imageView_image = (ImageView) findViewById(R.id.fragmentinscription_imageview_image);
                imageView_image.setImageBitmap(_iamgeBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Tabs_FragmentConnexion(), "Connexion");
        adapter.addFragment(new Tabs_FragmentInscription(), "Inscription");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }




}
