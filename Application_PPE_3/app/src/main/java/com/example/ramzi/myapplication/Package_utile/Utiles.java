package com.example.ramzi.myapplication.Package_utile;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by ramzi on 28/03/16.
 */
public class Utiles
{

    public String isNetworkAvailable(Context _context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null && activeNetworkInfo.isConnected())
        {
            return "1";
        }
        return "0";
    }

    public static Bitmap StringToBitMap(String _imageCompresse)
    {
        try {
            byte[] imageAsBytes = Base64.decode(_imageCompresse.getBytes(), Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

        } catch(Exception e)
        {
            e.getMessage();
            return null;
        }
    }
    public static String BitmapToString(Bitmap _image)
    {
        ByteArrayOutputStream bYtE = new ByteArrayOutputStream();
        _image.compress(Bitmap.CompressFormat.JPEG, 40, bYtE);
        _image.recycle();
        byte[] byteArray = bYtE.toByteArray();
        String imageFile = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return imageFile;
    }


}
