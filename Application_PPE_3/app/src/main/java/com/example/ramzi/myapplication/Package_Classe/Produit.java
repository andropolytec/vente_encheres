package com.example.ramzi.myapplication.Package_Classe;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ramzi on 25/03/16.
 */

public class Produit
{
    public static ArrayList<Produit> _List_PRODUIT_ordinateur = new ArrayList<Produit>();
    public static ArrayList<Produit> _List_PRODUIT_telephonie = new ArrayList<Produit>();
    public static ArrayList<Produit> _List_PRODUIT_jeuxvideos = new ArrayList<Produit>();

    public static ArrayList<Produit> _List_MES_PRODUIT = new ArrayList<Produit>();

    private String _id_produit;
    private String _libelle;
    private String _prix;
    private String _tempsExpiration;
    private String _image;
    private String _caracteristique;
    private String _id_categorie;
    private String _id_personne;
    private String _valider;
    private String _date_validation;

    public Produit(String _id_produit, String _libelle, String _prix, String _tempsExpiration, String _image,
                   String _caracteristique, String _id_categorie, String _id_personne,String _valider, String _date_validation) {

        this._id_produit = _id_produit;
        this._libelle = _libelle;
        this._prix = _prix;
        this._tempsExpiration = _tempsExpiration;
        this._image = _image;
        this._caracteristique = _caracteristique;
        this._id_categorie = _id_categorie;
        this._id_personne = _id_personne;
        this._valider = _valider;
        this._date_validation = _date_validation;
    }

    public String get_valider() {
        return _valider;
    }

    public void set_valider(String _valider) {
        this._valider = _valider;
    }

    public String get_id_produit() {
        return _id_produit;
    }

    public String get_libelle() {
        return _libelle;
    }

    public String get_prix() {
        return _prix;
    }

    public String get_tempsExpiration() {
        return _tempsExpiration;
    }

    public String get_image() {
        return _image;
    }

    public String get_caracteristique() {
        return _caracteristique;
    }

    public String get_id_categorie() {
        return _id_categorie;
    }

    public String get_id_personne() {
        return _id_personne;
    }

    public String get_date_validation() {
        return _date_validation;
    }

    public void set_id_produit(String _id_produit) {
        this._id_produit = _id_produit;
    }

    public void set_libelle(String _libelle) {
        this._libelle = _libelle;
    }

    public void set_prix(String _prix) {
        this._prix = _prix;
    }

    public void set_tempsExpiration(String _tempsExpiration) {
        this._tempsExpiration = _tempsExpiration;
    }

    public void set_image(String _image) {
        this._image = _image;
    }

    public void set_caracteristique(String _caracteristique) {
        this._caracteristique = _caracteristique;
    }

    public void set_id_categorie(String _id_categorie) {
        this._id_categorie = _id_categorie;
    }

    public void set_id_personne(String _id_personne) {
        this._id_personne = _id_personne;
    }

    public void set_date_validation(String _date_validation) {
        this._date_validation = _date_validation;
    }
}
